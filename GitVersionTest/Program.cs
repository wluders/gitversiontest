﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitVersionTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //release changes
             
            // some change here...
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // changes on master

            Application.Run(new Form1());

            //more changes
            //more changes.....

            //changes on release branch...

            // more changes on release branch
                
            //changes on other release

            //changes

             
            //
        
        }
    }
}
